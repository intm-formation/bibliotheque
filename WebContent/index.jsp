<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Bienvenue dans l'application Biblioth�que</h1>
<form method="get">
Recherche dans le titre: 
<input type="text" name="query"/>
<input type="submit"/>
</form>
<br>
<form method="get">
Recherche dans les auteurs: 
<input type="text" name="author"/>
<input type="submit"/>
</form>

<%

String requete= "";
if ( request.getParameter("query") != null)
{
	String recherche= request.getParameter("query");
	if( recherche != null){
		out.print( "Recherche de : "+recherche);
		// Recherche dans la base de donn�es
		requete= "select * from articles where titre like '%"+recherche+"%';" ;
	}
}

if ( request.getParameter( "author") != null)
{
	String recherche= request.getParameter("author").toLowerCase();
	if( recherche != null){
		requete= "select * from articles where lower( auteur_nom)= '"+recherche
			+"' or lower( auteur_prenom)= '"+recherche
			+"' or lower( autre_auteur_nom)= '"+recherche
			+"' or lower( autre_auteur_prenom)= '"+recherche+"';";
			
	}
}

if( requete != ""){
	try {
		//Class.forName("org.postgresql.Driver");
		Class.forName(getServletContext().getInitParameter("JDBCDriver"));
		out.print( "<br>Driver trouv�");
	
			Connection conn= DriverManager.getConnection(
				getServletContext().getInitParameter("JDBCURL"),
				getServletContext().getInitParameter("JDBCUser"),
				getServletContext().getInitParameter("JDBCPassword"));
		Statement stmt = conn.createStatement();
		ResultSet res= stmt.executeQuery(requete);
		while( res.next()){
			out.write( "<br>"+res.getString( 1)
				+" - "+res.getString(6)+" "+res.getString(5));

			// Le second auteur le cas �ch�ant
			if( res.getString( 9) != null || res.getString(8) != null)
				out.write( " & ");
			if( res.getString( 9) != null)
				out.write( " "+res.getString(9));
			if( res.getString( 8) != null)
				out.write( " "+res.getString(8));
			
			out.write(" - <a href=\"detail?codebar="+res.getString( 1)+"\">"+res.getString(3)+"</a>");
			
		}
		conn.close();
	} catch( ClassNotFoundException e) {
		out.write( "<br>Pb driver JDBC: "+e.getMessage());
		e.printStackTrace();
	} catch( SQLException e) {
		out.write( "<br>Exception SQL: "+e.getErrorCode()+"<br>"+e.getMessage());
	}
}

%>
</body>
</html>