<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>D�tail dun livre</title>
</head>
<body>
<H3>D�tail d'un livre</H3>

<% bibliotheque.beans.Article a= (bibliotheque.beans.Article)request.getAttribute( "Article"); %>


<table border="0" width="100%">
<tr><td colspan="3" align="center" bgcolor="#eeeeff">
<H1><%= a.getTitre() %>
<% if( a.getComplement()!= null)
	out.write( "<br>"+a.getComplement());
%></H1>
</td></tr>
<tr><td colspan="2">
<br>Auteur: 
<%
if( a.getAuteurPrenom()!= null)
	out.print(a.getAuteurPrenom()+" ");
if( a.getAuteurNom()!= null)
	out.print(a.getAuteurNom()+" ");
if( a.getAuteurQualif()!= null)
	out.print("("+a.getAuteurQualif()+") ");
if( a.getAutreAuteurPrenom() != null || a.getAutreAuteurNom() != null || a.getAutreAuteurQualif()!= null){
	out.print( "et ");
	if( a.getAutreAuteurPrenom()!= null)
		out.print(a.getAutreAuteurPrenom()+" ");
	if( a.getAutreAuteurNom()!= null)
		out.print(a.getAutreAuteurNom()+" ");
	if( a.getAutreAuteurQualif()!= null)
		out.print("("+a.getAutreAuteurQualif()+") ");
}

%>

</td><td rowspan="7" align="center">
<img src="http://covers.openlibrary.org/b/isbn/
<%= a.getIsbn().replace("-", "") %>
-L.jpg">

</td></tr>
<tr><td colspan="2">
<br>Editeur: <%= a.getEditeur() %>
</td></tr><tr><td colspan="2">
<br>Ann�e: <%= a.getAnnee() %>
</td></tr><tr><td>
<br>Type doc: <%= a.getTypeDoc() %>
</td><td align="center" rowspan="3">
Bar code de l'article:<br>
<img src="<%= "https://barcode.tec-it.com/barcode.ashx?data="+a.getCodebar()+"&code=Code128&translate-esc=on" %>">

</td></tr><tr><td>
<br>Section: <%= a.getSection() %>
</td></tr><tr><td>
<br>Cote: <%= a.getCote1() %>
</td></tr><tr><td colspan="2">
<br>Num�ro ISBN: <%= a.getIsbn() %>
</td></tr>
</table>
</body>
</html>