package bibliotheque.servlet;

import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Import du package du JavaBean
import bibliotheque.beans.Article;

/**
 * Servlet implementation class detail
 */
@WebServlet(description = "D�tail d'une fiche de livre", urlPatterns = { "/detail" })
public class detail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out= response.getWriter();
		
		// R�cup�ration des infos du livre
		String requete= "select * from articles where codebar="+request.getParameter("codebar") ;
		out.print("Requ�te SQL: "+requete);
		try {
			//Class.forName("org.postgresql.Driver");
			Class.forName(getServletContext().getInitParameter("JDBCDriver"));
			
			Connection conn= DriverManager.getConnection(
					getServletContext().getInitParameter("JDBCURL"),
					getServletContext().getInitParameter("JDBCUser"),
					getServletContext().getInitParameter("JDBCPassword"));
			Statement stmt = conn.createStatement();
			ResultSet res= stmt.executeQuery(requete);
			if ( res.next()){
				// Mise du r�sultat en attributs pour les passer � la page JSP
				Article a= new Article();
				a.setCodebar( res.getLong( 1));
				a.setIsbn( res.getString( 2));
				a.setTitre( res.getString( 3));
				a.setComplement( res.getString( 4));
				a.setAuteurNom( res.getString( 5)); 
				a.setAuteurPrenom( res.getString( 6));
				a.setAuteurQualif( res.getString( 7));
				a.setAutreAuteurNom( res.getString( 8));
				a.setAutreAuteurPrenom( res.getString( 9));
				a.setAutreAuteurQualif( res.getString( 10));
				a.setEditeur( res.getString( 11));
				a.setAnnee( res.getInt( 12));
				a.setTypeDoc( res.getString( 13));
				a.setSection( res.getString( 14));
				a.setCote1( res.getString( 15));
				//a.setCote2( res.getString( 16));
				request.setAttribute("Article", a);
				// Cha�nage sur la JSP
				getServletContext().getRequestDispatcher("/WEB-INF/detail.jsp").forward(request, response);
				
			} else
				out.write("Livre non trouv�.");
			conn.close();
		} catch( ClassNotFoundException e) {
			out.write( "<br>Pb driver JDBC: "+e.getMessage());
			e.printStackTrace();
		} catch( SQLException e) {
			out.write( "<br>Exception SQL: "+e.getErrorCode()+"<br>"+e.getMessage());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
