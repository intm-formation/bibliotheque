package bibliotheque.servlet;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;


public class Test extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		
	@Override
	public void doGet( HttpServletRequest req, HttpServletResponse res)  
											throws ServletException, IOException {
		res.setContentType("text/html");
		res.setCharacterEncoding( "UTF-8" );
		Writer o= res.getWriter();
		o.write("<h1>Test de connexion sur la base de donn�es avec JDBC</h1>");
		
		try {
			//Class.forName("org.postgresql.Driver");
			Class.forName(getServletContext().getInitParameter("JDBCDriver"));
			o.write( "<br>Driver trouv�");
			
			Connection conn= DriverManager.getConnection(
					getServletContext().getInitParameter("JDBCURL"),
					getServletContext().getInitParameter("JDBCUser"),
					getServletContext().getInitParameter("JDBCPassword"));
			
			conn.close();
		} catch( ClassNotFoundException e) {
			o.write( "<br>Pb driver JDBC: "+e.getMessage());
			e.printStackTrace();
		} catch( SQLException e) {
			o.write( "<br>Exception SQL: "+e.getErrorCode()+"<br>"+e.getMessage());
		}
		
	}
}
