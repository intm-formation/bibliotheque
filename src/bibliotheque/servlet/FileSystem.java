package bibliotheque.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;


/**
 * Servlet implementation class FileSystem
 */
@WebServlet("/files/*")
public class FileSystem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileSystem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter o= response.getWriter();
		o.append("<h1>Liste des fichiers</h1>");
		
		o.append( "<br>Servlet context path: "+getServletContext().getContextPath());
		String webAppPath= getServletContext().getRealPath("");
		String webAppUrlFile= request.getPathInfo(); 
		
		o.append( "<br>Real Path: "+webAppPath);
		o.append( "<br>URL Path: "+ webAppUrlFile);
		File f= new File( webAppPath + webAppUrlFile);
		//o.append( "<H2>"+f.getCanonicalPath() +"</H2>");
		o.append( "<H2>"+webAppUrlFile +"</H2>");
		o.append( "<br>DIR -- <a href=\""+getServletContext().getContextPath()+"/files"+webAppUrlFile.substring(0, webAppUrlFile.lastIndexOf("/")) +"\">..</a>" );
		
		String[] fics= f.list();
		for( String fic: fics) {
			o.append( "<br>");
			File fi= new File(f.getCanonicalPath()+"\\"+fic);
			String relativeUrl= getServletContext().getContextPath()+"/files"+webAppUrlFile;
			if( relativeUrl.charAt(relativeUrl.length()-1) != '/')
				relativeUrl+="/";
			relativeUrl+=fi.getName();
			if( fi.isDirectory()) o.append("DIR -- ");
				o.append( "<a href=\""+relativeUrl+"\">"+relativeUrl+"</a>");
			//else
			//	o.append( fi.getName());
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
