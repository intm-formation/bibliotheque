package bibliotheque.beans;

import java.sql.*;

/**
 * Article est le JavaBean contenant les donn�es d'un article de la m�diath�que
 * 
 * @author jbou
 *
 */
public class Article {
	// Propri�t�s
	private long codebar;
	private String isbn;
	private String titre;
	private String complement; 
	private String auteurNom;
	private String auteurPrenom;
	private String auteurQualif;
	private String autreAuteurNom;
	private String autreAuteurPrenom;
	private String autreAuteurQualif;
	private String editeur;
	private int annee;
	private String typeDoc;
	private String section;
	private String cote1;
	private String cote2;
	
	
	// Constructeur 
	public Article( long codebar) {
		
	}
	
	public Article() {
		
	}
	/*
	// M�thodes
	public Connection connect() {
		//Class.forName("org.postgresql.Driver");
		Class.forName(getServletContext().getInitParameter("JDBCDriver"));
		
		Connection conn= DriverManager.getConnection(
				getServletContext().getInitParameter("JDBCURL"),
				getServletContext().getInitParameter("JDBCUser"),
				getServletContext().getInitParameter("JDBCPassword"));
		return conn;
	}
	*/
	// Sauvegarde des donn�es du JavaBean dans la base de donn�es.
	public void save() {
		
	}
	// Accesseurs

	public long getCodebar() {
		return codebar;
				
	}

	public void setCodebar(long codebar) {
		this.codebar = codebar;
		String requ= "select * from articles where codebar= "+codebar;
		
}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getAuteurNom() {
		return auteurNom;
	}

	public void setAuteurNom(String auteurNom) {
		this.auteurNom = auteurNom;
	}

	public String getAuteurPrenom() {
		return auteurPrenom;
	}

	public void setAuteurPrenom(String auteurPrenom) {
		this.auteurPrenom = auteurPrenom;
	}

	public String getAuteurQualif() {
		return auteurQualif;
	}

	public void setAuteurQualif(String auteurQualif) {
		this.auteurQualif = auteurQualif;
	}

	public String getAutreAuteurNom() {
		return autreAuteurNom;
	}

	public void setAutreAuteurNom(String autreAuteurNom) {
		this.autreAuteurNom = autreAuteurNom;
	}

	public String getAutreAuteurPrenom() {
		return autreAuteurPrenom;
	}

	public void setAutreAuteurPrenom(String autreAuteurPrenom) {
		this.autreAuteurPrenom = autreAuteurPrenom;
	}

	public String getAutreAuteurQualif() {
		return autreAuteurQualif;
	}

	public void setAutreAuteurQualif(String autreAuteurQualif) {
		this.autreAuteurQualif = autreAuteurQualif;
	}

	public String getEditeur() {
		return editeur;
	}

	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public String getTypeDoc() {
		return typeDoc;
	}

	public void setTypeDoc(String typeDoc) {
		this.typeDoc = typeDoc;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getCote1() {
		return cote1;
	}

	public void setCote1(String cote1) {
		this.cote1 = cote1;
	}
	

	public String getCote2() {
		return cote2;
	}

	public void setCote2(String cote2) {
		this.cote2 = cote2;
	}
	
	
}
